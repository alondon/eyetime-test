import 'package:flutter/material.dart';
import 'package:flutter_test_for_eye_time_app/NewsHeadline.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

void main() => runApp(
  MaterialApp(
    home: EyetimeTest(),
  )
);

class EyetimeTest extends StatefulWidget {
  @override
  _EyetimeTestState createState() => _EyetimeTestState();
}

class _EyetimeTestState extends State<EyetimeTest> {

  Future<List<NewsHeadline>> showHeadlines() async {
    var data = await http.get("https://newsapi.org/v2/top-headlines?sources=bbc-news&apiKey=ed7c08e7956b46dd818738549af8a66d");
    var decoded1 = json.decode(data.body);
    print("Processing...");
    var decoded = decoded1['articles'];
    print(' ${decoded.length}');

    List<NewsHeadline> headlines = List();
    for (var headline in decoded) {
      headlines.add(NewsHeadline(headline['title'], headline['description']));
    }
    return headlines;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
          future: showHeadlines(),
          builder:(context,snapshot) {
            if (snapshot.hasData) {
              return CustomScrollView(
                  slivers: <Widget>[
                    SliverAppBar(
                      expandedHeight: 220,
                      flexibleSpace: FlexibleSpaceBar(
                        title: Text("EyeTime Test App"),
                      ),
                      pinned: true,
                    ),
                    SliverList(
                        delegate: SliverChildBuilderDelegate(
                              (context, index) {
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Card(
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      16.0, 16.0, 16.0, 60.0),
                                  child: Text(snapshot.data[index].title,
                                    style: TextStyle(fontSize: 18.0),),
                                ),
                              ),
                            );
                          },
                          childCount: snapshot.data.length,
                        )
                    ),
                  ]
              );
            } else {
              return Align(
                alignment: FractionalOffset.center,
                child: CircularProgressIndicator(),
              );
            }
          }
      )
    );
  }
}


